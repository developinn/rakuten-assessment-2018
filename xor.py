# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(M, N):
    answer = 0
    counter = 0
    A = int(M)
    B = int(N)
    while not (A == 0 and  B == 0):
        # print((A, B))
        bit = (not A % 2 == B % 2)
        if bit:
            answer += 2**counter
        A = int(A / 2)
        B = int(B / 2)
        counter += 1
        # print(answer)

    return answer

# def solution(M, N):
#     # write your code in Python 3.6
#     answer = 0
#     counter = 0
#     A = int(M)
#     B = int(N)
#     while not (A == 0 and  B == 0):
#         # print((A, B))
#         bit = True
#         for C in range(A, B + 1):
#             bit = bit and (not C % 2 == int(bit == 'true'))
#         if bit:
#             answer += 2**counter
#         A = int(A / 2)
#         B = int(B / 2)
#         counter += 1
#         # print(answer)

#     return answer

if __name__ == "__main__":
    print(solution(5, 8))
    # print(solution(12, 21))