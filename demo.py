# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    temp = []
    for a in A: 
        if a in temp:
            temp.remove(a)
        elif not a + 1 in temp:
            temp.append(a + 1)
        print(temp)
    return temp[0]

if __name__ == "__main__":
    print(solution([1, 3, 6, 4, 1, 2]))